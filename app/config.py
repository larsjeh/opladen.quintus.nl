import os
from datetime import timedelta

class Config(object):
	DEBUG = False
	TESTING = False

	BASE_DIR = os.path.abspath(os.path.dirname(__file__))
	THREADS_PER_PAGE = 2

	CSRF_ENABLED     = True
	CSRF_SESSION_KEY = "YBWjhrsXyV3rmsujgCGZhaHSRkQUUvwLfgMdpx9Wd3b4SA9Y"

	# Secret key for signing cookies
	SECRET_KEY = "YBWjhrsXyV3rmsujgCGZhaHSRkQUUvwLfgMdpx9Wd3b4SA9Y"
	JSONSCHEMA_DIR = os.path.join(BASE_DIR, 'Deposit/Schema')
	SQLALCHEMY_DATABASE_URI = 'mysql://portal:portal_dev@localhost/portal'
	#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
	SQLALCHEMY_TRACK_MODIFICATIONS = True

class ProductionConfig(Config):
	REDIRECT_AFTER_MOLLIE_URL = 'http://opladen.quintus.dev/payment_completed.html'
	RPI_URL = 'https://ae00379b98904073bd148e38c2173102331379dd4405a4c126a16920198f84.resindevice.io'
	SQLALCHEMY_DATABASE_URI = 'mysql://root:@db/portal'
	#DEBUG = True
	pass

class DevelopmentConfig(Config):
	RPI_URL = 'https://ae00379b98904073bd148e38c2173102331379dd4405a4c126a16920198f84.resindevice.io'
	REDIRECT_AFTER_MOLLIE_URL = 'http://opladen.quintus.dev/payment_completed.html'
	DEBUG = True
	BASE_DIR = os.path.abspath(os.path.dirname(__file__))

class TestingConfig(Config):
	TESTING = True
	SQLALCHEMY_DATABASE_URI = 'sqlite://'  #in memory
	SECRET_KEY = "TEST"

class StagingConfig(Config):
	DEVELOPMENT = True
	DEBUG = True
