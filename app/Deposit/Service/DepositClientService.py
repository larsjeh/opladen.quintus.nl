from Deposit.Blueprints.Status.StatusModel import Status
from Deposit.factory import db
from flask import current_app as app
import datetime
import requests
import json
import threading
import time

# error handling
from Deposit.Exceptions.InvalidUsageException import InvalidUsageException

from Deposit.Exceptions.PortalNotAvailableException import PortalNotAvailableException

class DepositClientService():
    def __init__(self):
        pass

    def platform_available(self):
        ##TODO UITBREIDEN
        last_status = db.session.query(Status).order_by(Status.id.desc()).first()
        if last_status is None or (datetime.datetime.now() - last_status.date_created) > datetime.timedelta(seconds=30):
            app.logger.critical('No status received from RPI')
            return False
        if last_status.status != "connected":
            app.logger.critical('RPI unable to connect to ETC server')
            return False
        return True

    def repeat_webhook(self,url, payment_id,logger):
        i = 1

        while i <= 5:
            payload = {
                "payment_id": payment_id
            }

            headers = {
                'content-type': 'application/json'
            }


            r = requests.post(url, data=json.dumps(payload),headers=headers )

            if r.status_code == 200:
                logger.critical("RPI webhook resolved after " + str(i) + " tries.")
                return
            else:
                i += 1
            time.sleep(30)
        logger.fatal("RPI cannot be reached after "+i+ " tries. Closing portal")
        #WEBHOOK CANNOT BE CALLED. RASPBERRY PI DOWN? ##INJECT LOGGER OR FIRE SIGNAL (PEW PEW)

    def webhook(self,payment_id ):
        payload = {
            "payment_id": payment_id
        }

        headers = {
            'content-type': 'application/json'
        }

        url = app.config['RPI_URL'] + '/api/webhook'
        app.logger.info('Calling URL: ' + url)
        app.logger.info('Calling RPI webhook for payment_id ' + str(payment_id))
        r = requests.post(url, data= json.dumps(payload), headers=headers )

        if r.status_code == 200:
            app.logger.info('RPI webhook call succesful for payment_id' + str(payment_id))
            return r
        else:
            app.logger.critical("RPI webhook can not be called, started retrying in separate thread")
            t = threading.Thread(target=self.repeat_webhook, args=(url,payment_id,app.logger,))
            t.daemon = True
            t.start()
            return r


    def get_card(self,card_id):
        app.logger.info('Retrieving ' + card_id + ' from ETC')

        url = app.config['RPI_URL'] + '/api/cards/' + str(card_id)
        r = requests.get(url)

        app.logger.info("Getting info for card_id: "+ card_id)
        app.logger.debug(r.text)

        if r.status_code == 400:
            app.logger.warning('Card retrieval from ETC failed for card_id ' + str(card_id))
            raise InvalidUsageException('No valid card')
        if r.status_code != 200:
            app.logger.warning('Error retrieving card '+str(card_id) +'from RPI ')
            raise PortalNotAvailableException('')

        app.logger.info('Card retrieval from ETC succesful for card_id ' + str(card_id))

        return json.loads(r.text)