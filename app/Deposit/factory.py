from flask import Flask, request, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_jsonschema import ValidationError
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity
from raven.contrib.flask import Sentry
from flask_cors import CORS, cross_origin
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import logging
import config
from logging.handlers import SysLogHandler

app = Flask(__name__)
db = SQLAlchemy()
jwt = JWTManager()
sentry = Sentry(
    dsn='https://41916c8c66ee432bb79ce6048091faab:1975292296e4424f9d689b8f154d022a@sentry.io/136265')
limiter = Limiter(key_func=get_remote_address)

from Deposit.LoggingHandlers.MailgunHandler import MailgunHandler


def create_app(config_filename=config.TestingConfig):
    app = Flask(__name__)
    limiter.init_app(app)
    #migrate = Migrate(app, db)
    CORS(app)
    app.config.from_object(config_filename)
    db.init_app(app)

    # blueprint imports
    from Deposit.Blueprints.Payment.PaymentController import payments
    from Deposit.Blueprints.Status.StatusController import status

    # Register Blueprints
    app.register_blueprint(payments)
    app.register_blueprint(status)

    # ERROR HANDLING
    from Deposit.Exceptions.ErrorFallbacks import wrongRequest, notAuthorized, accessDenied, notFound, notAllowed, \
        serverError, handle_error
    app.register_error_handler(400, wrongRequest)
    app.register_error_handler(401, notAuthorized)
    app.register_error_handler(403, accessDenied)
    app.register_error_handler(404, notFound)
    app.register_error_handler(405, notAllowed)
    app.register_error_handler(500, serverError)

    # ERROR HANDLING
    from Deposit.Exceptions.NotFoundException import NotFoundException
    from Deposit.Exceptions.InvalidUsageException import InvalidUsageException
    from Deposit.Exceptions.PortalNotAvailableException import PortalNotAvailableException
    from Deposit.Exceptions.DuplicateException import DuplicateException

    app.register_error_handler(InvalidUsageException, handle_error)
    app.register_error_handler(PortalNotAvailableException, handle_error)
    app.register_error_handler(ValidationError, wrongRequest)
    app.register_error_handler(DuplicateException, handle_error)
    app.register_error_handler(NotFoundException, handle_error)

    # LOGGING
    sentry.init_app(app)
    app.logger.addHandler(get_mailgun_handler())


    #syslog_handler = SysLogHandler(facility=SysLogHandler.LOG_DAEMON, address="/dev/log")
    #syslog_handler.setLevel(logging.DEBUG)
    #syslog_handler.setFormatter(logging.Formatter('''
    #        Message type:       %(levelname)s
    #        Location:           %(pathname)s:%(lineno)d
    #        Module:             %(module)s
    #        Function:           %(funcName)s
    #        Time:               %(asctime)s
    #
    #        Message:
    #
    #        %(message)s
    #    '''))
    #app.logger.addHandler(syslog_handler)

    with app.app_context():
        db.create_all()
        try:
            pass
        except Exception as e:
            print 'Initiated the database'

    return app

##TODO SPLIT UP
def get_mailgun_handler():
    # Setup logging handler
    mail_handler = MailgunHandler(
        api_url='https://api.mailgun.net/v3/sandbox8c669a211e604df6a30ec57e8025b12e.mailgun.org',
        api_key='key-dd1f709c84d7582591cab23788ad5f91',
        sender='postmaster@sandbox8c669a211e604df6a30ec57e8025b12e.mailgun.org',
        recipients=['l.k.hopman@gmail.com'],
        subject='Application error!'
    )
    mail_handler.setLevel(logging.CRITICAL)
    mail_handler.setFormatter(logging.Formatter('''
        Message type:       %(levelname)s
        Location:           %(pathname)s:%(lineno)d
        Module:             %(module)s
        Function:           %(funcName)s
        Time:               %(asctime)s

        Message:

        %(message)s
    '''))
    return mail_handler
