from Deposit.factory import db
import isodate


class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())


class Payment(Base):
    __tablename__ = 'transactions'
    __table_args__ = {'extend_existing': True}

    # mandatory
    status = db.Column(db.String(255), nullable=False)
    card_id = db.Column(db.String(255), nullable=False)
    created_datetime = db.Column(db.DateTime, nullable=False)
    amount = db.Column(db.Integer, nullable=False)

    # from mollie
    description = db.Column(db.String(255), nullable=True)
    method = db.Column(db.String(255), nullable=True)
    mollie_id = db.Column(db.String(255), nullable=True)
    mode = db.Column(db.String(255), nullable=True)
    paid_datetime = db.Column(db.String(255), nullable=True)
    consumer_account = db.Column(db.String(255), nullable=True)
    consumer_bic = db.Column(db.String(255), nullable=True)
    consumer_name = db.Column(db.String(255), nullable=True)
    money_added_to_card = db.Column(db.Boolean)

    # New instance instantiation procedure
    def __init__(self, card_id, status, created_datetime, amount):
        self.card_id = card_id
        self.status = status
        self.created_datetime = created_datetime
        self.amount = amount
        self.money_added_to_card = False

    def __repr__(self):
        return '<Payment %r>' % (self.id)

    def fill_with_mollie_response(self, data):
        if 'status' in data:
            self.status = data['status']
        if 'description' in data:
            self.description = data['description']
        if 'method' in data:
            self.method = data['method']
        if 'id' in data:
            self.mollie_id = data['id']
        if 'mode' in data:
            self.mode = data['mode']
        if 'paidDatetime' in data:
            self.paid_datetime = data['paidDatetime']
        if 'details' in data and data['details'] is not None:
            if 'consumerAccount' in data['details']:
                self.consumer_account = data['details']['consumerAccount']
            if 'consumerBic' in data['details']:
                self.consumer_bic = data['details']['consumerBic']
            if 'consumerName' in data['details']:
                self.consumer_name = data['details']['consumerName']

    def to_dict(self):
        return {
            'payment_id': self.id,
            'status': self.status,
            'card_id': self.card_id,
            'created_datetime': self.created_datetime,
            'amount': self.amount,
            'method': self.method,
            'money_added_to_card':self.money_added_to_card
        }
