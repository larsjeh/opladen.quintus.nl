# Import flask dependencies
from flask import Blueprint, request, redirect, request, Response, jsonify, abort
from Deposit.factory import db, limiter
from flask import current_app as app
import Mollie
import datetime
import time
import json
import requests

# models
from Deposit.Blueprints.Payment.PaymentModel import Payment
from Deposit.Decorators.ValidateJsonSchema import json_schema_required
from Deposit.Decorators.CheckDepositClientIsUp import portal_is_available

# error handling
from Deposit.Exceptions.NotFoundException import NotFoundException
from Deposit.Exceptions.PortalNotAvailableException import PortalNotAvailableException
from Deposit.Exceptions.InvalidUsageException import InvalidUsageException
# service
from Deposit.Service.DepositClientService import DepositClientService


# Define the blueprint
payments = Blueprint('payments', __name__)

mollie = Mollie.API.Client()
mollie.setApiKey('test_KustFeabVArtr9gJGy8CdAmz9Bnxka')  # MOVE TO ENV VARIABLES



@limiter.limit("100/day")
@limiter.limit("50/hour")
@limiter.limit("10/minute")
@payments.route('/api/cards/<card_id>', methods=['GET'])
@portal_is_available
def get_card_details(card_id):
    """
    Return 503 if platform not available
    Return 400  if card cannot be loaded
    Return 200 if card can be loaded and normal response from ETC
    """

    deposit_client = DepositClientService()
    card = deposit_client.get_card(card_id)

    app.logger.debug(card)

    return jsonify(card), 200


@json_schema_required('payments/create_payment.json')
@payments.route('/api/payments', methods=['POST'])
@portal_is_available
def send_payment_request():
    """
    Return 503 if platform not available
    Return 400  if card cannot be loaded
    Return 201 if Payment is created
    """
    try:
        data = request.get_json(silent=True)
        if data['amount'] < 500:
            raise InvalidUsageException('A minimum amount of 5 euro is needed')

        deposit_client = DepositClientService()
        card = deposit_client.get_card(data['card_id'])

        # Create Payment in own db
        payment = Payment(data['card_id'], 'started', datetime.datetime.utcnow(), data['amount'])
        db.session.add(payment)
        db.session.commit()

        # get public URL from Ngrok admin API (Assumes there is only one tunnel
        # active)
        # TODO: REMOVE NGROK HACK AFTER DEV
        url = 'https://4648f3e7.ngrok.io'
        #resp = requests.get(url='http://localhost:4040/api/tunnels')
        #data_ngrok = json.loads(resp.text)
        #url = data_ngrok['tunnels'][0]['public_url']
        # TODO: REMOVE NGROK HACK AFTER DEV


        mollie_payment = mollie.payments.create({
            'amount': float(data['amount']) / 100,
            'description': 'Opladen pasnummer ' + str(data['card_id']) + ' SESQ',
            'webhookUrl': url + '/api/webhook',
            'redirectUrl': app.config['REDIRECT_AFTER_MOLLIE_URL'] + '?payment_id=' + str(payment.id),
            'metadata': {
                'card_id': data['card_id'],
                'payment_id': payment.id
            }
        })

        payment_url = {'payment_url': mollie_payment.getPaymentUrl()}
        json_response = dict(payment_url.items() + payment.to_dict().items())

        return jsonify(json_response), 201
    except Mollie.API.Error as e:
        raise PortalNotAvailableException('Mollie not available')



@payments.route('/api/webhook', methods=['POST'])
def webhook():
    try:
        if 'id' not in request.form:
            raise NotFoundException('Payment not found')

        mollie_payment = mollie.payments.get(request.form['id'])
        payment = Payment.query.get(mollie_payment['metadata']['payment_id'])
        app.logger.info('Processing webhook with payment_id: '+ str(mollie_payment['metadata']['payment_id']))
        if mollie_payment.isPaid() and payment is not None:
            # Fill entity with new information from Mollie
            payment.fill_with_mollie_response(mollie_payment)
            db.session.commit()

            deposit_client = DepositClientService()
            deposit_client.webhook(payment.id)

        return jsonify(None), 200

    except Mollie.API.Error as e:
        return 'API call failed: ' + e.message


@payments.route('/api/payments/<int:payment_id>', methods=['GET'])
@limiter.limit("100/day")
@limiter.limit("50/hour")
@limiter.limit("20/minute")
def get_payment(payment_id):
    """
    Return 404 if Payment cannot be found
    Return 200  if Payment is returned
    """

    payment = Payment.query.get(payment_id)
    app.logger.debug(payment.to_dict())
    if payment is None:
        raise NotFoundException('Payment not found')

    return jsonify(payment.to_dict()), 200

#TODO AUTHENTICATIE
@json_schema_required('payments/create_payment.json')
@payments.route('/api/payments/<int:payment_id>', methods=['PUT'])
def put_payment(payment_id):
    """
    Return 404 if Payment cannot be found
    Return 200  if Payment is returned
    """

    payment = Payment.query.get(payment_id)
    if payment is None:
        raise NotFoundException('Payment not found')

    data = request.get_json(silent=True)
    if 'money_added_to_card' in data:
        payment.money_added_to_card = data['money_added_to_card']
    db.session.commit()

    return jsonify(payment.to_dict()), 200
