# Import flask dependencies
from flask import Blueprint, request, redirect, Response, jsonify, abort
from Deposit.factory import db
from Deposit.Decorators.ValidateJsonSchema import json_schema_required
from flask import current_app as app

import datetime
# models
from Deposit.Blueprints.Status.StatusModel import Status

# Define the blueprint
status = Blueprint('status', __name__)

@status.route('/api/status', methods=['POST'])
@json_schema_required('/Status/create_status.json')
def status_update():
    data = request.get_json(silent=True)
    status = data['status']

    # Create transaction in own db
    status = Status(status)
    db.session.add(status)
    db.session.commit()

    return jsonify(status.to_dict()), 201
