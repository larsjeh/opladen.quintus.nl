from Deposit.factory import db

class Base(db.Model):
	__abstract__  = True

	id			= db.Column(db.Integer, primary_key=True)
	date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
										   onupdate=db.func.current_timestamp())

class Status(Base):
	__tablename__ = 'status'
	__table_args__ = {'extend_existing': True}

	#mandatory
	status = db.Column(db.String(255), nullable=False)

	def __init__(self, status):
		self.status = status
	def to_dict(self):
		return {
			'status': self.status,
			'date_created': self.date_created,
			'id': self.id,
			'date_modified': self.date_modified
		}

