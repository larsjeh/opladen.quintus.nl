from flask import request
from functools import wraps
from flask import current_app as app
import json
import jsonschema

def json_schema_required(path_to_json):
	def decorator(f):
		@wraps(f)
		def wrapper(*args, **kw):
			schema = open(app.config['JSONSCHEMA_DIR'] + path_to_json).read()
			data = request.get_json(silent=True)
			jsonschema.validate(data, json.loads(schema))
			return f(*args, **kw)
		return wrapper
	return decorator