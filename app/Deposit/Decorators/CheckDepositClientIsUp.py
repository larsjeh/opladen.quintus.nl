from flask import request
from functools import wraps
from flask import current_app as app
import json
import jsonschema


from Deposit.Exceptions.PortalNotAvailableException import PortalNotAvailableException
from Deposit.Service.DepositClientService import DepositClientService

portal = DepositClientService()

def portal_is_available(f):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if not portal.platform_available():
                app.logger.critical('VERY BIG PROBLEMZZ')
                raise PortalNotAvailableException('Portal is currently not available')
            return f(*args, **kwargs)

        return wrapped

    return wrapper(f)