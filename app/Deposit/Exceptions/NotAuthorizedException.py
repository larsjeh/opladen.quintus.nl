from flask import jsonify

class NotAuthorizedException(Exception):

	def __init__(self):
		Exception.__init__(self)
		self.status_code = 403

	def to_dict(self):
		rv = {'message' : 'Not allowed to access this resource'}
		return rv
