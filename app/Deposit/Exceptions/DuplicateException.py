from flask import jsonify

class DuplicateException(Exception):

	def __init__(self):
		Exception.__init__(self)
		self.status_code = 409

	def to_dict(self):
		rv = {'message' : 'Duplicate resource'}
		return rv
