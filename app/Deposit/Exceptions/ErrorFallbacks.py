from flask import jsonify

def wrongRequest(error):
    return jsonify({"message": "Wrong request"}), 400

def notAuthorized(error):
    return jsonify({"message": "Not authorized"}), 401

def accessDenied(error):
    return jsonify({"message": "Access denied"}), 403

def notFound(error):
    return jsonify({"message": "Not found"}), 404

def notAllowed(error):

    return jsonify({"message": "Not allowed"}), 404
def serverError(error):

    return jsonify({"message": "Server error"}), 500


def handle_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
